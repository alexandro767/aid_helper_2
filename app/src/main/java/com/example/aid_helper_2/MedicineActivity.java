package com.example.aid_helper_2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aid_helper_2.R;
import com.example.aid_helper_2.model.DataBase;
import com.example.aid_helper_2.model.Medicine;
import com.example.aid_helper_2.model.Medicine_Dao;
import com.example.aid_helper_2.model.Recepy;
import com.example.aid_helper_2.model.Singleton;

import java.util.Calendar;
import java.util.Date;

// TODO: 01.03.2021 сверять коррекность даты из интернета

public class MedicineActivity extends AppCompatActivity {
    private DataBase dataBase;
    private long medicine_id;
    private Medicine medicine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_medicine_create);

        dataBase = Singleton.getDataBase(MedicineActivity.this);
        if((medicine_id = getIntent().getLongExtra(Medicine.MEDICINE_ID, -1)) != -1) {
            medicine = dataBase.medicine_Dao().getMedicineById(medicine_id);
            if(medicine == null) {
                Toast.makeText(this, getString(R.string.error_no_medicine), Toast.LENGTH_LONG).show();
            }
        }

        Button ok = findViewById(R.id.ok_btn);
        EditText medicine_name = findViewById(R.id.medicine_name);
        TextView end_date = findViewById(R.id.medicine_end_date);
        DatePicker date_picker = findViewById(R.id.med_date_picker);
        Calendar calendar = Calendar.getInstance();
        RadioGroup alert_period_rg = findViewById(R.id.alert_period_rg);
        for(Medicine.ALERT_PERIOD period : Medicine.ALERT_PERIOD.values()) {
            RadioButton button = new RadioButton(MedicineActivity.this);
            button.setId(period.view_id);
            button.setText(period.toString());
            alert_period_rg.addView(button);
        }
        date_picker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                end_date.setText(Singleton.sql_format.format(new Date(calendar.getTimeInMillis())));
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder errors = new StringBuilder();
                if(alert_period_rg.getCheckedRadioButtonId() == -1) {
                    errors.append(getString(R.string.error_alert_period));
                }
                if(medicine_name.length() == 0) {
                    if(errors.length() > 0) {
                        errors.append("\n");
                    }
                    errors.append(getString(R.string.error_name));
                }
                if(end_date.getText().length() == 0) {
                    if(errors.length() > 0) {
                        errors.append("\n");
                    }
                    errors.append(getString(R.string.error_end_date));
                }
                if(errors.length() > 0) {
                    Toast.makeText(MedicineActivity.this, errors.toString(), Toast.LENGTH_LONG).show();
                    return;
                }


                if(medicine == null) {
                    dataBase.medicine_Dao().insert(
                        new Medicine(
                                medicine_name.getText().toString(),
                                Singleton.getMedicine_next_id(),
                                1,
                                end_date.getText().toString(),
                                Medicine.ALERT_PERIOD.find_by_id(alert_period_rg.getCheckedRadioButtonId()).name()
                        )
                    );
                } else {
                    medicine.setName(medicine_name.getText().toString());
                    medicine.setAlert_date(end_date.getText().toString(), Medicine.ALERT_PERIOD.find_by_id(alert_period_rg.getCheckedRadioButtonId()).name());


                    dataBase.medicine_Dao().update(medicine);
                }
                MedicineActivity.this.finish();
            }
        });
        Button cancel = findViewById(R.id.cancel_btn);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MedicineActivity.this.finish();
            }
        });

        if(medicine == null) { //заполняем значение с переданного элемента
            return;
        }

        medicine_name.setText(medicine.name);
        end_date.setText(medicine.getEnd_date());
        Date date = new Date();
        calendar.setTime(date);
        // TODO: 13.03.2021 задать дату в календаре по умолчанию
        for(int i = 0; i < alert_period_rg.getChildCount(); i++) {
            // TODO: 13.03.2021 доработать заполнение этого элемента
            ((RadioButton)alert_period_rg.getChildAt(i)).getText().toString().equals(medicine.getAlert_period());
        }
    }
}
package com.example.aid_helper_2;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.aid_helper_2.model.DataBase;
import com.example.aid_helper_2.model.Medicine;
import com.example.aid_helper_2.model.Medicine_List_Adapter;
import com.example.aid_helper_2.model.Recepy;
import com.example.aid_helper_2.model.Recepy_List_Adapter;
import com.example.aid_helper_2.model.Singleton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private Recepy_List_Adapter recepy_adapter;
    private Medicine_List_Adapter medicine_adapter;
    private DataBase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, Recepy_List_Activity.class);
            startActivity(intent);
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_recepies, R.id.nav_medicines)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        dataBase = Singleton.getDataBase(this);

        //generateExample();


        // РАБОТА С РЕЦЕПТАМИ
        // TODO: 03.03.2021 Фильтрация рецептов после базовой - пользователь сам перетаскивает их вверх и вниз по списку, новосозданный рецепо появляется сверху
        ListView recepies = findViewById(R.id.recepy_list);
        recepy_adapter = new Recepy_List_Adapter(dataBase.recepy_Dao().getAll(), this);
        //recepy_adapter.addAll(dataBase.recepy_Dao().getAll());
        recepies.setAdapter(recepy_adapter);

        Button recepy_create_btn = findViewById(R.id.create_recepy);
        recepy_create_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recepy_intent = new Intent(MainActivity.this, RecepyActivity.class);
                startActivity(recepy_intent);

            }
        });

        Button recepyButton = findViewById(R.id.hide_recepy_btn);
        recepyButton.setOnClickListener(new View.OnClickListener() {   //анонимный класс
            @Override
            public void onClick(View view) {
                recepies.setVisibility(recepies.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
            }
        });



        // РАБОТА С ЛЕКАРСТВАМИ
        // TODO: 02.03.2021 мб стоит добавить показ даты начала предупреждений
        // TODO: 02.03.2021 возможность редактирования + одинаковый экран для редактирования и создания - да, экран одинаковый, редактирование по свайпу влево и выбору иконки редактирования
        // TODO: 02.03.2021 возможность удаления (архив?) - пользователь самостоятельно удаляет эти лекарства
        // TODO: 02.03.2021 дальнейшая сортровка активных и неактивных рецептов - гитхаб

        // TODO: 03.03.2021 Изменить функциональность кнопки свернуть/развернуть по аналогии с ПМ
        // TODO: 03.03.2021 Окно для добавления- именно как отдельный экран, для унитарности
        // TODO: 03.03.2021 Метод организации данных- такой, чтобы можно было создавать резерв.копию и восстанавливаться из нее
        // TODO: 03.03.2021 ПОДУМАЙ НАД РАЗМЕТКОЙ И КОГДА СТОИТ ЕЕ СДЕЛАТЬ, ЭКРАНЫ- ТО УНИТАРНЫЕ
        ListView medicines = findViewById(R.id.medicine_list);
        medicine_adapter = new Medicine_List_Adapter(dataBase.medicine_Dao().getAll(), this);
        //medicine_adapter.addAll(dataBase.medicine_Dao().getAll());
        medicines.setAdapter(medicine_adapter);

        Button medicine_create_btn = findViewById(R.id.add_medicine);
        medicine_create_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent medicine_intent = new Intent(MainActivity.this, MedicineActivity.class);
                startActivity(medicine_intent);
            }
        });

        Button medicineButton = findViewById(R.id.hide_medicine_btn);
        medicineButton.setOnClickListener(new View.OnClickListener() {   //анонимный класс
            @Override
            public void onClick(View view) {
                medicines.setVisibility(medicines.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //recepy_array = dataBase.recepy_Dao().getAll();
        recepy_adapter.setRecepy_array(dataBase.recepy_Dao().getAll());
        recepy_adapter.notifyDataSetChanged();

        //medicine_adapter.;
        medicine_adapter.setMedicine_array(dataBase.medicine_Dao().getAll());
        medicine_adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /*@Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.nav_recepies:
                Intent intent = new Intent(this, RecepyActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void generateExample() {
        for(int i = 0; i < 10; i++) {
            dataBase.medicine_Dao().insert(new Medicine(
                    String.format("med%s", i),
                    Singleton.getMedicine_next_id(),
                    1,
                    Singleton.sql_format.format(new Date()),
                    Medicine.ALERT_PERIOD.WEEK.name())
            );
        }
        for(int i = 0; i < 10; i++) {
            dataBase.recepy_Dao().insert(new Recepy(
                    String.format("rec%s", i),
                    Recepy.State.ACTIVE.name(),
                    Singleton.getRecepy_next_id(),
                    1
                    )
            );
        }
    }
}
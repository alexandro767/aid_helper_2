package com.example.aid_helper_2;

import android.content.Intent;
import android.os.Bundle;

import com.example.aid_helper_2.model.DataBase;
import com.example.aid_helper_2.model.Medicine_List_Adapter;
import com.example.aid_helper_2.model.Singleton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class Medicine_List_Activity extends AppCompatActivity {
    private Medicine_List_Adapter medicine_adapter;
    private DataBase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine__list_);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        dataBase = Singleton.getDataBase(this);
        ListView medicines = findViewById(R.id.medicine_list);
        medicine_adapter = new Medicine_List_Adapter(dataBase.medicine_Dao().getAll(), this);
        //medicine_adapter.addAll(dataBase.medicine_Dao().getAll());
        medicines.setAdapter(medicine_adapter);

        Button medicine_create_btn = findViewById(R.id.add_medicine);
        medicine_create_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent medicine_intent = new Intent(Medicine_List_Activity.this, MedicineActivity.class);
                startActivity(medicine_intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        medicine_adapter.setMedicine_array(dataBase.medicine_Dao().getAll());
        medicine_adapter.notifyDataSetChanged();
    }
}
package com.example.aid_helper_2;

import android.os.Bundle;

import com.example.aid_helper_2.model.Recepy;
import com.example.aid_helper_2.model.Singleton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class RecepyActivity extends AppCompatActivity {
private boolean status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_recepy_create);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        CheckBox status_checkbox = findViewById(R.id.status_checkbox);
        status_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status_checkbox.setText(status_checkbox.isChecked() ? R.string.inactive : R.string.active);
                status_checkbox.setChecked(!status_checkbox.isChecked());
                RecepyActivity.this.onResume();
            }
        });
        EditText recepy_name = findViewById(R.id.recepy_name);
        Button ok = findViewById(R.id.ok_btn);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recepy_name.getText().toString().length() != 0) {
                    //adapter.add(new Recepy(recepy_name.getText().toString(), Recepy.State.ACTIVE, id, model.getCurrentUserID()));
                    Singleton.getDataBase(RecepyActivity.this).recepy_Dao().insert(
                            new Recepy(
                                    recepy_name.getText().toString(),
                                    "active",
                                    Singleton.getRecepy_next_id(),
                                    1));
                    RecepyActivity.this.finish();
                }
            }
        });
        Button cancel = findViewById(R.id.cancel_btn);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecepyActivity.this.finish();
            }
        });
    }
}
package com.example.aid_helper_2;

import android.content.Intent;
import android.os.Bundle;

import com.example.aid_helper_2.model.DataBase;
import com.example.aid_helper_2.model.Medicine_List_Adapter;
import com.example.aid_helper_2.model.Recepy_List_Adapter;
import com.example.aid_helper_2.model.Singleton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class Recepy_List_Activity extends AppCompatActivity {
    private Recepy_List_Adapter recepy_adapter;
    private DataBase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recepy__list_);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        dataBase = Singleton.getDataBase(this);
        ListView recepies = findViewById(R.id.recepy_list);
        recepy_adapter = new Recepy_List_Adapter(dataBase.recepy_Dao().getAll(), this);
        //medicine_adapter.addAll(dataBase.medicine_Dao().getAll());
        recepies.setAdapter(recepy_adapter);

        Button recepy_create_btn = findViewById(R.id.create_recepy);
        recepy_create_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recepy_intent = new Intent(Recepy_List_Activity.this, RecepyActivity.class);
                startActivity(recepy_intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //recepy_array = dataBase.recepy_Dao().getAll();
        recepy_adapter.setRecepy_array(dataBase.recepy_Dao().getAll());
        recepy_adapter.notifyDataSetChanged();
    }
}
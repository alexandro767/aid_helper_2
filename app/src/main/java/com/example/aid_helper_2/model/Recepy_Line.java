package com.example.aid_helper_2.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Recepy_Line {
    @PrimaryKey(autoGenerate = true)
    public long recepy_line_id;

    @ColumnInfo
    public final long recepy_id;

    @Embedded
    private Medicine medicine;

    @ColumnInfo(name = "shedule")
    private String shedule;

    public Recepy_Line(long recepy_id, Medicine medicine, String shedule) {
        this.recepy_id = recepy_id;
        this.medicine = medicine;
        this.shedule = shedule;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public String getShedule() {
        return shedule;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    public void setShedule(String shedule) {
        this.shedule = shedule;
    }
}

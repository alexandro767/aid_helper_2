package com.example.aid_helper_2.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import java.util.List;

/**Рецепт- перечень лекарств и инструкция по их применению (когда + сколько)*/
@Entity
public class Recepy {
    public static final String RECEPY_ID = "recepy_id";

    @PrimaryKey
    public final long id;

    @ColumnInfo(name = "name")
    String name;

    @ColumnInfo(name = "status")
    String status;

    @ColumnInfo(name = "user_id")
    public final long user_id;

    /*@Relation(parentColumn = "id", entityColumn = "recepy_id")
    public List<Recepy_Line> recepy_line_list;*/

    public Recepy(String name, String status, long id, long user_id) {
        this.name = name;
        this.status = status;
        this.id = id;
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return name;
    }

    public enum State {ACTIVE {
        @NonNull
        @Override
        public String toString() {
            return "активный";
        }
    }, INACTIVE {
        @NonNull
        @Override
        public String toString() {
            return "неактивный";
        }
    }};
}

package com.example.aid_helper_2.model;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.room.Dao;

import com.example.aid_helper_2.MainActivity;
import com.example.aid_helper_2.MedicineActivity;
import com.example.aid_helper_2.R;
import com.example.aid_helper_2.RecepyActivity;

import java.util.List;

public class Medicine_List_Adapter extends BaseAdapter {
    private static LayoutInflater inflater = null;

    private List<Medicine> medicine_array;
    private Context context;

    public Medicine_List_Adapter(List<Medicine> medicine_array, Context context) {
        this.medicine_array = medicine_array;
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setMedicine_array(List<Medicine> medicine_array) {
        this.medicine_array = medicine_array;
    }

    @Override
    public int getCount() {
        return medicine_array.size();
    }

    @Override
    public Object getItem(int position) {
        return medicine_array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return medicine_array.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null) {
            vi = inflater.inflate(R.layout.medicine_line, null);
        }
        TextView text1 = (TextView) vi.findViewById(R.id.name);
        text1.setText(medicine_array.get(position).name);
        TextView text2 = (TextView) vi.findViewById(R.id.alert_date);
        text2.setText(medicine_array.get(position).getAlert_date());
        TextView text3 = (TextView) vi.findViewById(R.id.end_date);
        text3.setText(medicine_array.get(position).getEnd_date());
        Button delete_btn = vi.findViewById(R.id.delete_btn);
        delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Singleton.getDataBase(context).medicine_Dao().delete(medicine_array.get(position));
            }
        });
        Button edit_btn = vi.findViewById(R.id.edit_btn);
        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent medicine_edit_intent = new Intent(context, MedicineActivity.class);
                //Bundle bundle = new Bundle();
                //bundle.putInt(Medicine.MEDICINE_ID, (int)getItemId(position));
                medicine_edit_intent.putExtra(Medicine.MEDICINE_ID, getItemId(position));
                context.startActivity(medicine_edit_intent);
            }

        });
        return vi;
    }
}

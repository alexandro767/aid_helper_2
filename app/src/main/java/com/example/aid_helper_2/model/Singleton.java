package com.example.aid_helper_2.model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;

import java.text.SimpleDateFormat;

public class Singleton {
    static private long recepy_next_id;
    static private long medicine_next_id;
    private static DataBase dataBase;
    public final static SimpleDateFormat sql_format = new SimpleDateFormat("yyyy.MM.dd");

    public static DataBase getDataBase(Context context) {
        if(dataBase != null) {
            return dataBase;
        }

        dataBase = Room.databaseBuilder(context.getApplicationContext(), DataBase.class, "database-name").allowMainThreadQueries().build();
        recepy_next_id = dataBase.recepy_Dao().cur_max_id() + 1;
        medicine_next_id = dataBase.medicine_Dao().cur_max_id() + 1;

        return dataBase;
    }

    static public long getRecepy_next_id() {
        return recepy_next_id++;
    }

    static public long getMedicine_next_id() {
        return medicine_next_id++;
    }

}

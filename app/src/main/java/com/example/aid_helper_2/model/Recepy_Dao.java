package com.example.aid_helper_2.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface Recepy_Dao {
    @Query("SELECT * FROM Recepy order by status")
    List<Recepy> getAll();

    @Query("SELECT * FROM Recepy WHERE user_id = :userId")
    Recepy loadById(long userId);

    @Query("SELECT MAX (id) FROM recepy")
    long cur_max_id();

    @Insert
    void insert(Recepy recepy);

    @Delete
    void delete(Recepy recepy);

}

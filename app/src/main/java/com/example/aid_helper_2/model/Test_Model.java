package com.example.aid_helper_2.model;

import java.util.ArrayList;
import java.util.List;

public class Test_Model implements Model_Interface {
    @Override
    public List<Recepy> getAllRecepies(int user_id) {
        List<Recepy> recepy_array = new ArrayList<>();
        /*recepy_array[0] = new Recepy("Кагоцел", Recepy.State.ACTIVE, 1, user_id);
        recepy_array[1] = new Recepy("Капотен", Recepy.State.ACTIVE, 2, user_id);
        recepy_array[2] = new Recepy("Валидол", Recepy.State.ACTIVE, 3, user_id);*/
        return recepy_array;
    }

    @Override
    public List<Medicine> getAllMedicines(int user_id) {
        List<Medicine> medicine_array = new ArrayList<>();
        /*medicine_array[0] = new Medicine("Лекарство 1", 1, user_id);
        medicine_array[1] = new Medicine("Лекарство 2", 2, user_id);
        medicine_array[2] = new Medicine("Лекарство 3", 3, user_id);*/
        return medicine_array;
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<User>();
    }

    @Override
    public boolean createNewRecepy(Recepy recepy) {
        return true;
    }

    @Override
    public boolean createNewMedicine(Medicine medicine) {
        return true;
    }

    @Override
    public boolean createNewUser(User user) {
        return true;
    }

    @Override
    public int getCurrentUserID() {
        return 1;
    }

    @Override
    public int setCurrentUserID(int id) {
        return id;
    }
}

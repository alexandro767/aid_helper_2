package com.example.aid_helper_2.model;

public class User {
    public final String name;
    public final int id;

    public User(String name, int id) {
        this.name = name;
        this.id = id;
    }
}

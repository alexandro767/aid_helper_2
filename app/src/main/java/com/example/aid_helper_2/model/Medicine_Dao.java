package com.example.aid_helper_2.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface Medicine_Dao {
    @Query("SELECT * FROM Medicine")
    List<Medicine> getAll();

    @Query("SELECT * FROM Medicine WHERE user_id = :userId")
    Medicine loadById(long userId);

    @Query("SELECT * FROM Medicine WHERE id = :id")
    Medicine getMedicineById(long id);

    @Query("SELECT MAX (id) FROM medicine")
    long cur_max_id();

    @Insert
    void insert(Medicine medicine);

    @Delete
    void delete(Medicine medicine);

    @Update
    void update(Medicine medicine);
}

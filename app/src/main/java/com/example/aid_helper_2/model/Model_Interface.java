package com.example.aid_helper_2.model;

import java.util.List;

public interface Model_Interface {
    public List<Recepy> getAllRecepies(int user_id);
    public List<Medicine> getAllMedicines(int user_id);
    public List<User> getAllUsers();
    public boolean createNewRecepy(Recepy recepy);
    public boolean createNewMedicine(Medicine medicine);
    public boolean createNewUser(User user);
    public int getCurrentUserID();
    public int setCurrentUserID(int id);
}

package com.example.aid_helper_2.model;

import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Recepy.class, Medicine.class}, version = 1)
public abstract class DataBase extends RoomDatabase{
    public abstract Recepy_Dao recepy_Dao();
    public abstract Medicine_Dao medicine_Dao();
}

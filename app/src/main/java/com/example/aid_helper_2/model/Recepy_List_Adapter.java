package com.example.aid_helper_2.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.aid_helper_2.R;

import java.util.List;

public class Recepy_List_Adapter extends BaseAdapter {
    private static LayoutInflater inflater = null;

    private List<Recepy> recepy_array;
    private Context context;

    public Recepy_List_Adapter(List<Recepy> recepy_array, Context context) {
        this.recepy_array = recepy_array;
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setRecepy_array(List<Recepy> recepy_array) {
        this.recepy_array = recepy_array;
    }

    @Override
    public int getCount() {
        return recepy_array.size();
    }

    @Override
    public Object getItem(int position) {
        return recepy_array.get(position);
    }

    @Override
    public long getItemId(int position) {
        return recepy_array.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null) {
            vi = inflater.inflate(R.layout.recepy_line, null);
        }
        TextView text1 = (TextView) vi.findViewById(R.id.name_recepy);
        text1.setText(recepy_array.get(position).name);
        return vi;
    }
}

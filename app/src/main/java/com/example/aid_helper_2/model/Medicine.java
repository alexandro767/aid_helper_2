package com.example.aid_helper_2.model;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

@Entity
public class Medicine {
    public static final String MEDICINE_ID = "medicine_id";

    @PrimaryKey
    public final long id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "user_id")
    public final long user_id;

    @ColumnInfo
    private String end_date;

    @ColumnInfo
    private String alert_period; // 3 значения: day, week, month

    @ColumnInfo
    private String alert_date;

    public String getEnd_date() {
        return end_date;
    }

    public String getAlert_period() {
        return alert_period;
    }

    public String getAlert_date() {
        return alert_date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAlert_date(String end_date, String alert_period) {
        this.alert_date = getAlert_date(end_date, alert_period);
        this.end_date = end_date;
        this.alert_period = alert_period;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
        this.alert_date = getAlert_date(end_date, alert_period);
    }

    public void setAlert_period(String alert_period) {
        this.alert_period = alert_period;
        this.alert_date = getAlert_date(end_date, alert_period);
    }

    public void setAlert_date(String alert_date) {
        this.alert_date = alert_date;
    }

    public enum ALERT_PERIOD {
        DAY(Calendar.DAY_OF_YEAR, 1, View.generateViewId()) {
            @NonNull
            @Override
            public String toString() {
                return "День";
            }
        }, WEEK(Calendar.WEEK_OF_YEAR, 7, View.generateViewId()) {
            @NonNull
            @Override
            public String toString() {
                return "Неделя";
            }
        }, MONTH(Calendar.MONTH, 30, View.generateViewId()) {
            @NonNull
            @Override
            public String toString() {
                return "30 дней";
            }
        };

        public final int calendar_period, days_cnt, view_id;

        ALERT_PERIOD(int calendar_period, int days_cnt, int view_id) {
            this.calendar_period = calendar_period;
            this.days_cnt = days_cnt;
            this.view_id = view_id;
        }

        public static ALERT_PERIOD find_by_id(int id) {
            for(ALERT_PERIOD period : ALERT_PERIOD.values()) {
                if (period.view_id == id) {
                    return period;
                }
            }
            return null;
        }
    }

    public Medicine(String name, long id, long user_id, String end_date, String alert_period) {
        this.name = name;
        this.id = id;
        this.user_id = user_id;
        this.end_date = end_date;
        this.alert_period = alert_period;
        this.alert_date = getAlert_date(end_date, alert_period);
    }

    private String getAlert_date(String end_date, String alert_period) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(Singleton.sql_format.parse(end_date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.add(ALERT_PERIOD.valueOf(alert_period).calendar_period, -1);
        return Singleton.sql_format.format(new Date(calendar.getTimeInMillis()));
    }

    @Override
    public String toString() {
        return name;
    }
}

